Sebastian Latacz
Windows

Results:

- seperate commands in example
- add comment in example that echo is only for creating test file
- fix windows beeping
- fix windows highlighting
- proposal: bear rating - no idea what bear to use
- hint to check the "languages" tab below the example

Test:

- gets code from dropbox
- visits coala.io
- `pip3 install coala-bears`
- "Scheint zu funktionieren, das finde ich ziemlich ueberraschend"
- scrolls to "getting started"
- reads python example
- "no idea what the "echo ..." does, the coala line probably runs coala"
    - comment from notetaker: does the echo "..." > ... work on windows?
- read the full tutorial
- searches docs for "python"
- getting started with coala
- back to coala.io
- cd into the code dir, install is complete
- "I guess I have to be in the directory to run it"
- `coala --files euler10.py --bears PEP8Bear, PyUnusedCodeBear`
- Laptop beeps loudly and weirdly, tester fears for his life
- Code is unreadable due to syntax highlighting on windows
- Reads:
    - "This code does not comply to PEP8"
    - "[NORMAL] PEP8Bear"
    - This doesn't look like in the code!
    - Sees the diff "there's the code again"
    - Realizes that the diff shows changes
    - "Why is the result so weird? That's not how my code looks." confused
- Apply patch
- Open file: "atom"
- "Windows can't find the file"
- next result: tester reports to understand the structure
- "Why do I have to do something after applying the patch?"
- coala run done
- "There's two files! The orig is probably the backup."
- "The new doesn't seem to have changed." - restarts editor
- "Why is the file not changed?" - editor not updated
- Compares new to original
    - "Introduced newlines but didn't advertise to do that, maybe I didn't see something? :/"
- "Now I wanna try something!"
- tries around
- "Why does it allow one or two newlines before the for loop in module code?" (note: seems both allowed)
- does more newlines
- works
- "Two seem to be allowed"
- "Cool!"
