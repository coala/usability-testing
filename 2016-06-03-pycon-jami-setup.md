Subject: coala installation
Version: N/A
Platform: Windows

- User copies the entire 'cd project && …' line, there's no project directory,
  gets error.
- Fixes this instantly by realizing her project dir goes there, but does not
  change the `**/*.py` even though she has a JS project.
